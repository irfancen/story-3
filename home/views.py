from django.shortcuts import render

def index(request):
    return render(request, 'index.html')

def secret(request):
    return render(request, 'secret.html')