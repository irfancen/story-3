var scrolling = false;
var prevScrollpos = window.pageYOffset;
var content = document.getElementById('cont-nav');
var opened = false;
var x = window.matchMedia("(min-width: 769px)").addListener(checkMobile)

window.smoothScroll = function(target) {
    scrolling = true;
    var scrollContainer = target;
    do { // find scrollContainer
        scrollContainer = scrollContainer.parentNode;
        if (!scrollContainer) return;
        scrollContainer.scrollTop += 1;
    } while (scrollContainer.scrollTop == 0);

    var targetY = 0;
    do { // find top of target relative to the container
        if (target == document.getElementById('home')) {
            target.offsetParent = 0;
            break;
        }
        if (target == scrollContainer) break;
        targetY += target.offsetTop;
    } while (target = target.offsetParent);

    scroll = function(c, a, b, i) {
        i += 0.5;
        if (i > 30) return;
        c.scrollTop = a + (b - a) / 30 * i;
        setTimeout(function() {
            scroll(c, a, b, i);
        }, 5)
    }
    // Scroll
    scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
    navbar();
    setTimeout(function() {
        hideable();
        }, 500)

}

hideable = function() {
    scrolling = false;
}

window.onscroll = function() {
    if (!scrolling) {
        checkScroll();
    }
}

window.checkScroll = function() {
    var currentScrollPos = window.pageYOffset;
        if (content.style.display != "block") {
            if (prevScrollpos >= currentScrollPos) {
                document.getElementById("navbar").style.top = "0";
            }
            else {
                document.getElementById("navbar").style.top = "-100px";
            }
            prevScrollpos = currentScrollPos;
        }
}


navbar = function() {
    if (window.matchMedia("(max-width: 768px)").matches) {
        if (content.style.display == "none") {
            content.style.display = "block";
            content.style.animation = "open 0.5s ease-out"
        } else {
            content.style.animation = "close 0.5s ease-out"
            setTimeout(function() {
                content.style.display = "none";
            }, 400)
            
        }
    }
}

function checkMobile(x) {
    if (x.matches) {
        content.style.display = "initial";
    } else {
        content.style.display = "none";
    }
}